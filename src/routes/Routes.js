import { useEffect } from "react";
import {
  Route,
  Routes,
  Navigate,
  useLocation,
  useNavigate,
} from "react-router-dom";
import Dashboard from "../components/Dashboard/Dashboard";
import Issues from "../pages/Issues/Issues";
import Login from "../pages/Login/Login";

const Router = () => {
  const getData = JSON.parse(localStorage.getItem("userData"));
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (!getData && location.pathname !== "/") {
      window.location.replace("/");
    }
    
    if (getData && getData.Checkbox === true) {
      navigate("/dashboard");
    }
    else {
      navigate("/");
    }
  }, []);


  return (
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/issues" element={<Issues />} />
    </Routes>
  );
};

export default Router;
