import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import { useEffect, useState } from "react";
import NestedListItems from "../NestedListItems/NestedListItems";
import { useNavigate } from "react-router";

const NestedList = ({ item, toggleSidebar, route }) => {
  const [open, setOpen] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    setOpen(true);
  }, [toggleSidebar]);

  const handleClick = () => {
    setOpen(!open);
    navigate(item.route);
  };
  return (
    <>
      <ListItemButton
        onClick={handleClick}
        className={
          route === item.route
            ? "list-item-active list-item"
            : item.title == "Dashboard"
            ? "list-item"
            : toggleSidebar
            ? "list-item-no-border"
            : "list-item-border list-item"
        }
      >
        <ListItemIcon>{item.icon}</ListItemIcon>
        <ListItemText
          className={toggleSidebar ? `list-item-no-text` : ""}
          primary={item.title}
        />
      </ListItemButton>
      {item.subList !== undefined && (
        <Collapse in={true} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {item.subList.length > 0 &&
              item.subList.map((subItem, i) => {
                return (
                  <NestedListItems
                    key={i}
                    parentItem={item}
                    item={subItem}
                    index={subItem.title}
                    toggleSidebar={toggleSidebar}
                  />
                );
              })}
          </List>
        </Collapse>
      )}
    </>
  );
};

export default NestedList;
