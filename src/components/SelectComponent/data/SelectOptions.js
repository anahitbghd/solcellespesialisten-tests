export const SelectOptions = () => {
  return [
    {
      Priority: [
        {
          label: "Hige",
          value: "Hige",
        },
        {
          label: "Low",
          value: "Low",
        },
        {
          label: "Medium",
          value: "Medium",
        },
      ],
    },
    {
      SwitchStatus: [
        {
          label: "On",
          value: "On",
        },
        {
          label: "Off",
          value: "Off",
        },
      ],
    },
    {
      Status: [
        {
          label: "Active",
          value: "Active",
        },
        {
          label: "Inactive",
          value: "Inactive",
        },
      ],
    },
    {
      Role: [
        {
          label: "Admin",
          value: "Admin",
        },
        {
          label: "Standard",
          value: "Standard",
        },
        {
          label: "User",
          value: "User",
        },
      ],
    },
    {
      User: [
        {
          label: "John",
          value: "John",
        },
        {
          label: "Jacob",
          value: "Jacob",
        },
        {
          label: "Jennifer",
          value: "Jennifer",
        },
      ],
    },
  ];
};
