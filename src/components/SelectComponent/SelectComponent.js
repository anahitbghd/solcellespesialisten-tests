import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { Controller } from "react-hook-form";
import { SelectOptions } from "./data/SelectOptions";

const SelectComponent = ({ title, control, defaultValue,  }) => {
  return (
    <>
      <FormControl>
        <InputLabel id="demo-multiple-name-label">{title}</InputLabel>
        <Controller
          control={control}
          defaultValue={""}
          name={title}
          render={({ field, fieldState: { error }, }) => (
            <Select
              {...field}
              value={field.value}
              onChange={field.onChange}
              error={!!error}
              helpertext={error ? error.message : null}
              sx={{
                maxWidth: 400,
                width: "100%",
              }}
              labelId="demo-multiple-name-label"
              id="demo-multiple-name"
              label={title}
              className={
                title === "Role" || title === "User" ? "select-input" : ""
              }
            >
              {SelectOptions().map((options, index) => {
                if (options[title] !== undefined) {
                  return options[title].map((option) => {
                    return (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    );
                  });
                }
              })}
            </Select>
          )}
        rules={ (title === "Role" || title === "User") ? { required: `${title} reuired ` } : ""}
        />
      </FormControl>
    </>
  );
};

export default SelectComponent;
