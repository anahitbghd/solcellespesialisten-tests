import React, { useState, useEffect } from "react";
import { Box, List, ListItemButton, Grid } from "@mui/material";
import { useLocation, useNavigate } from "react-router-dom";
import { DashboardItemsNames } from "./data/DashboardItemsNames";
import NestedList from "../NestedList/NestedList";
import "./Dashboard.scss";

const Dashboard = ({ route, children }) => {
  const navigate = useNavigate();
  const location = useLocation();

  const [toggleSidebar, setToggleSidebar] = useState(true);

  return (
    <>
      <Grid container spacing={2} className="page-container">
        <Box className="header"></Box>
        <Box className="dashboard">
          <Grid
            container
            spacing={2}
            className="dashboard-container"
            sx={{ pt: "16px" }}
          >
            <Grid
              item
              className={`dashboard-container-left-side`}
              sx={{
                width: toggleSidebar ? "75px" : "260px"
              }}
              onMouseEnter={() => setToggleSidebar(false)}
              onMouseLeave={() => setToggleSidebar(true)}
            >
              <List
                className="list-container"
                component="nav"
                aria-labelledby="nested-list-subheader"
              >
                {DashboardItemsNames().map((item, index) => {
                  return (
                    <NestedList
                      key={index}
                      item={item}
                      toggleSidebar={toggleSidebar}
                      route={location.pathname}
                    />
                  );
                })}
              </List>
            </Grid>
            <Grid item className="dashboard-container-right-side">
              {children}
            </Grid>
          </Grid>
        </Box>
      </Grid>
    </>
  );
};

export default Dashboard;
