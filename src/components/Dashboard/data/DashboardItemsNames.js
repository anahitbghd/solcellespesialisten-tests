import { MapIcon } from "../../../assets/icons/MapIcon";
import { TachometerIcon } from "../../../assets/icons/TachometerIcon";
import { ListIcon } from "../../../assets/icons/ListIcon";
import { CircleIcon } from "../../../assets/icons/CircleIcon";
import { TasksIcon } from "../../../assets/icons/TasksIcon";
import { UsersIcon } from "../../../assets/icons/UsersIcon";
import { SettingIcon } from "../../../assets/icons/SettingIcon";
import { MoonIcon } from "../../../assets/icons/MoonIcon";
import AppearanceSwitch from "../../AppearanceSwitch";

export const DashboardItemsNames = () => {
  return [
    {
      title: "Dashboard",
      icon: <TachometerIcon />,
      route: '/dashboard'
    },
    {
      title: "Projects",
      icon: <ListIcon />,
      route: '/projects'
    },
    {
      title: "Issues",
      icon: <CircleIcon />,
      route: '/issues'
    },
    {
      title: "Map",
      icon: <MapIcon />,
      route: '/map'
    },
    {
      title: "Planning",
      icon: <TasksIcon />,
      route: '/planning',
      subList: [
        {
          title: "Gantt chart",
        },
        {
          title: "Calendar",
        },
      ],
    },
    {
      title: "Checklist",
      icon: <ListIcon />,
      route: '/checklist',
      subList: [
        {
          title: "Checklist",
        },
        {
          title: "Create checklists",
        },
      ],
    },
    {
      title: "Resources",
      icon: <UsersIcon />,
      route: '/resources',
      subList: [
        {
          title: "Teams",
        },
        {
          title: "Employees",
        },
      ],
    },
    {
      title: "Settings",
      icon: <SettingIcon />,
      route: '/settings',
      subList: [
        {
          title: "Partner settings",
        },
        {
          title: "Email settings",
        },
        {
          title: "Users",
        },
        {
          title: "Product settings",
        },
      ],
    },
    {
      title: "Appearance",
      icon: <MoonIcon />,
      route: '/appearance',
      subList: [
        {
          title: <AppearanceSwitch/>
        }
      ]
    },
    {
      title: "Offer",
      icon: <CircleIcon />,
      route: '/offer'
    },
  ];
};
