export const userData = () => {
  return [
    {
      id: "",
      name: "John",
      phone: "123-456-7890",
      email: "John.Abraham@joeabraham.com",
      role: "",
    },
    {
      id: "",
      name: "Jacob",
      phone: "123-456-7890",
      email: "Jacob.Abraham@joeabraham.com",
      role: "",
    },
    {
      id: "",
      name: "Jennifer",
      phone: "123-456-7890",
      email: "Jennifer.Abraham@joeabraham.com",
      role: "",
    },
  ];
};
