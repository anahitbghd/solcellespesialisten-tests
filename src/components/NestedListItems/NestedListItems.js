import { ListItemButton, ListItemText } from "@mui/material";
import React from "react";

const NestedListItems = ({ item, toggleSidebar }) => {
  return (
    <>
      <ListItemButton className="list-item-options" sx={{ pl: "70px" }}>
        {!toggleSidebar ? <ListItemText primary={item.title} /> : ""}
      </ListItemButton>
    </>
  );
};

export default NestedListItems;
