import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import loginImage from "../../assets/images/login-image.png";
import logoImage from "../../assets/images/sunflow-logo.png";
import "./Login.scss";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import VisibilityOffOutlinedIcon from "@mui/icons-material/VisibilityOffOutlined";
import { useEffect, useState } from "react";
import { UserIcon } from "../../assets/icons/UserIcon";
import { LockIcon } from "../../assets/icons/LockIcon";
import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router";
import bcrypt from "bcryptjs";

const Login = () => {
  const navigate = useNavigate();
  const { handleSubmit, reset, setValue, control } = useForm();
  let salt = bcrypt.genSaltSync(10);

  const [showPassword, setShowPassword] = useState(false);

  const ShowPasswordhandleClick = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmitData = (data) => {
    let hash = bcrypt.hashSync(data.password, salt);
    let newData = {...data, password : hash};
    localStorage.setItem("userData", JSON.stringify(newData));
    navigate("/dashboard");
  };

  return (
    <Box className="login-page">
      <Grid container spacing={2} className="login-page-container">
        <Grid item xs={4} className="login-page-container-left">
          <Box display="flex" justifyContent="flex-end" flexDirection="column">
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              flexDirection="column"
              sx={{ mt: "40px" }}
            >
              <img src={logoImage} alt="Sunflow" />
              <Typography variant="h6" component="h6">
                Log in to your account
              </Typography>
            </Box>
            <form
              onSubmit={handleSubmit((data) => {
                handleSubmitData(data);
              })}
            >
              <Controller
                name="username"
                control={control}
                defaultValue=""
                render={({
                  field: { onChange, value },
                  fieldState: { error },
                }) => (
                  <TextField
                    type="text"
                    variant="outlined"
                    className="primaryInput"
                    placeholder="Username"
                    value={value}
                    onChange={onChange}
                    error={!!error}
                    helperText={error ? error.message : null}
                    sx={{
                      input: {
                        color: "black",
                        "&::placeholder": {
                          opacity: 1,
                        },
                      },
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <UserIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
                rules={{ required: "Username required" }}
              />
              <Controller
                name="password"
                control={control}
                defaultValue=""
                render={({
                  field: { onChange, value },
                  fieldState: { error },
                }) => (
                  <TextField
                    type={showPassword ? "text" : "password"}
                    variant="outlined"
                    className="primaryInput"
                    placeholder="Password"
                    value={value}
                    onChange={onChange}
                    error={!!error}
                    helperText={error ? error.message : null}
                    sx={{
                      input: {
                        color: "black",
                        "&::placeholder": {
                          opacity: 1,
                        },
                      },
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <LockIcon />
                        </InputAdornment>
                      ),
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={ShowPasswordhandleClick}
                            edge="end"
                          >
                            {showPassword ? (
                              <VisibilityOffOutlinedIcon />
                            ) : (
                              <VisibilityOutlinedIcon />
                            )}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
                rules={{ required: "Password required" }}
              />
              <Box className="checkBoxContainer" sx={{ pl: "15px" }}>
                <Controller
                  control={control}
                  name={`Checkbox`}
                  defaultValue={false}
                  render={({ field: { onChange, value } }) => (
                    <FormControlLabel
                      label="Keep me logged in"
                      control={<Checkbox checked={value} onChange={onChange} />}
                    />
                  )}
                />
              </Box>
              <Button
                type="submit"
                className="primaryBtn primaryBtn--filled"
                variant="contained"
              >
                Login
              </Button>
            </form>
          </Box>
        </Grid>
        <Grid item xs={8} className="login-page-container-right">
          <Box className="right-side">
            <img src={loginImage} alt="Sunflow" />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Login;
