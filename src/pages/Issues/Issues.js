import {
  Box,
  Button,
  FormControlLabel,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { GreyCalendarIcon } from "../../assets/icons/GreyCalendarIcon";
import Dashboard from "../../components/Dashboard/Dashboard";
import SelectComponent from "../../components/SelectComponent/SelectComponent";
import "./Issues.scss";
import ColorSwitches from "../../components/PinkSwitch/PinkSwitch";
import EnhancedTable from "../../components/DataTable/DataTable";
import { v4 as uuidv4 } from "uuid";

const Issues = () => {
  const { handleSubmit, control, defaultValue, formState: { errors } } = useForm();
  const [newData, setNewData] = useState([]);
  const Logout = () => {
    localStorage.removeItem("userData");
    window.location.replace("/");
  };

  return (
    <Dashboard route="/issues">
      <Grid className="issues-container" container spacing={2}>
        <Box className="issues-header">
          <Button variant="outlined" onClick={Logout}>
            Log Out
          </Button>
          <Button variant="contained">Save Changes</Button>
        </Box>
        <Box className="issues-section">
          <Typography variant="h6" gutterBottom>
            Create an Issue
          </Typography>
          <Typography variant="body2" gutterBottom>
            Last updated on 09.08.2022 04:03 am
          </Typography>
          <form
            onSubmit={handleSubmit((data) => {
              let newData = { User: data.User, Role: data.Role };
              let uuid = uuidv4();
              sessionStorage.setItem(`${uuid}`, JSON.stringify(newData));
              const allFields = Object.entries(sessionStorage);
              if (allFields) {
                let newDataArray = [];
                allFields.map((fields) => {
                  let fieldsArray = JSON.parse(fields[1]);
                  let newFields = { ...fieldsArray, id: fields[0] };
                  if (!newDataArray.some((obj) => obj.id.includes(fields[0]))) {
                    newDataArray.push(newFields);
                  }
                });
                setNewData(newDataArray);
              }
            })}
          >
            <Box className="issues-section-first">
              <Controller
                name={"title"}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    id="outlined-helperText"
                    label="Title"
                    defaultValue={""}
                    onChange={onChange}
                    value={value}
                    sx={{
                      maxWidth: 400,
                      width: "100%",
                    }}
                  />
                )}
              />
              <SelectComponent
                title={"Priority"}
                control={control}
                defaultValue={defaultValue}
                name="priority"
              />
              <SelectComponent
                title={"SwitchStatus"}
                control={control}
                defaultValue={defaultValue}
              />
              <SelectComponent
                title={"Status"}
                control={control}
                defaultValue={defaultValue}
              />
              <FormControlLabel
                control={<ColorSwitches />}
                label="Electrical"
              />
              <Controller
                name={"description"}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    id="outlined-multiline-static"
                    className="description"
                    label="Description"
                    multiline
                    rows={3}
                    variant="outlined"
                    sx={{ width: "100%", mr: "20px" }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            edge="end"
                          >
                            <GreyCalendarIcon />
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
              />
              <Controller
                name={"Repair Date"}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    id="outlined-helperText"
                    label="Repair Date"
                    defaultValue={""}
                    // helperText="Some important text"
                    onChange={onChange}
                    value={value}
                    // size="small"
                    sx={{
                      maxWidth: 400,
                      width: "100%",
                    }}
                  />
                )}
              />
              <Controller
                name={"number"}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    id="outlined-number"
                    label="Time Estimate Hours"
                    type="number"
                    // size="small"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    sx={{
                      maxWidth: 400,
                      width: "100%",
                    }}
                  />
                )}
              />
              <FormControlLabel control={<ColorSwitches />} label="Hub" />
            </Box>
            <Typography variant="h6" gutterBottom className="issue-contact">
              Issue Contact
            </Typography>
            <Box className="issues-section-second">
              <SelectComponent
                title={"Role"}
                control={control}
                className="select-input"
                defaultValue={defaultValue}
                errors = {errors}
              />
              <SelectComponent
                title={"User"}
                control={control}
                className="select-input"
                defaultValue={defaultValue}
                errors = {errors}
              />
              <Button variant="contained" type="submit">
                Assign
              </Button>
            </Box>
          </form>
          <EnhancedTable newData={newData} setNewData={setNewData} />
        </Box>
      </Grid>
    </Dashboard>
  );
};

export default Issues;
